$(function(){
	$("#inloggen").on("click", function(){
		var user = $("#user").val();
		var pass = $("#pass").val();


		$.get("http://192.168.99.100:5000/user?name="+user+"&password="+pass, { name: user, password: pass}, function(data){
			/*console.log(data);*/
			data = JSON.parse(data);
			if(data.Code == "0"){
				$.cookie("ChatServiceUID", data.UID);
				window.location.replace(window.location.href + "chat.html");
			} else {
				$.removeCookie("ChatServiceUID");
			}
		});
	});
	/*console.log($.cookie("ChatServiceUID"));*/
	if($.cookie("ChatServiceUID") != undefined){
		loadChats($.cookie("ChatServiceUID"));
	}
});

function loadChats(uid){
	$.get("http://192.168.99.100:5000/getChats?uid="+uid, function(data){
/*		console.log(data);*/
		data = JSON.parse(data);
		if(data.Code == "0"){
			for(var key in data.Chats){
				var id;
				var name;
				if(data.Chats[key].RID != uid){
					id = data.Chats[key].RID;
					name = data.Chats[key].RName;
				} else {
					id = data.Chats[key].SID;
					name = data.Chats[key].SName;
				}
				$("#chats").append("<div class='chat' onclick='loadMessages(" + data.Chats[key].ID + ")'><label>" + name + "</label></div>");
			}
		} else {
			$("#chats").append("<div class='warning'>Het is niet gelukt om de chats te laden.</div>");
		}
	});
}

var messageInterval;
var scrollHeight;

function loadMessages(cid){
	$("#chat-screen").css("display", "block");
	stopInterval();
	var uid = $.cookie("ChatServiceUID");
	getMessages(cid);

	messageInterval = setInterval(function(){
		getMessages(cid);
	},1000);
	$("#send-message").css("display", "block");
	$("#send-new-message").attr("onclick", "sendMessage(" + cid + ", " + uid + ")");
}

function getMessages(cid){
	var uid = $.cookie("ChatServiceUID");
	$.get("http://192.168.99.100:5000/getMessages?cid="+cid, function(data){
		console.log(data);
		data = JSON.parse(data);
		if(data.Code == "0"){
			var id;
			var name;
			if(data.Chat.RID != uid){
				id = data.Chat.RID;
				name = data.Chat.RName;
			} else {
				id = data.Chat.SID;
				name = data.Chat.SName;
			}
			$("#chat-head").html("<label>" + name + "</label>");
			$(".chat-row").remove();
			for(var key in data.Messages){
				var alignment;
				if(data.Messages[key].UID != uid){
					alignment = "left";
				} else {
					alignment = "right";
				}
				$("#messages").append("<div class='chat-row " + alignment + "'><div class='chat-message card'>"+ data.Messages[key].Message + "</div></div>");
			}
			autoScroll("#messages");
		} else {
			$("#messages").html("<div class='warning'>Het is niet gelukt om de chat te laden.</div>");
		}
	});
	if($("#shadow").length < 1){
		$("#messages").append("<div id='shadow' class='shadow'></div>");
	}
}
function sendMessage(cid, sid){
	var message = $("#new-message").val().replace(" ", "+");
	$.get("http://192.168.99.100:5000/send?cid=" + cid + "&uid=" + sid + "&message="+message, function(data){
		/*console.log(data);*/
		data = JSON.parse(data);
		if(data.Code != "0"){
			$("#messages").append("<div class='warning'>Bericht niet verstuurd.</div>");
		} else {
			$("#new-message").val("");
		}
	});
}

function stopInterval(){
	clearInterval(messageInterval);
}

function autoScroll(div){
	var d = $(div);
	if(scrollHeight == null){
		scrollHeight = d.prop("scrollHeight");
		d.scrollTop(d.prop("scrollHeight"));
	}
	if(scrollHeight != d.prop("scrollHeight")){
		d.scrollTop(d.prop("scrollHeight"));
	}
}